﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerSide.Models
{
    public class TransactionModel
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public int CarId { get; set; }
        public float MoneyAmount { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}, Time: {Time.ToString("HH:mm:ss")}, CarId: {CarId}, MoneyAmount: {MoneyAmount}";
        }
    }
}
