﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ServerSide.Transport
{
    class Car : Transport
    {
        private static float Fee { get; set; }
        private static string Type { get; set; }
        static Car()
        {
            Fee = service.GetTariff().Car;
            Type = "Автомобіль";
        }

        public Car(float balance) : base(balance)
        { }

        public override float GetFee()
        {
            return Fee;
        }

        public override string GetTransportType()
        {
            return Type;
        }
    }
}
