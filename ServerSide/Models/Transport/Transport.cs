﻿using ServerSide.Models;
using System.Timers;

namespace ServerSide.Transport
{
    public abstract class Transport
    {
        protected static Services.SettingService service { get; }
        private static Services.TransactionService transactionService { get; }
        private static int Counter = 1;
        public int Id { get; protected set; }
        public float Balance { get; protected set; }
        private Timer timer { get; set; }  

        static Transport()
        {
            service = Services.SettingService.GetInstance();
            transactionService = Services.TransactionService.GetInstance();
        }

        protected Transport(float balance)
        {
            Id = Counter;
            Balance = balance;
            ++Counter;
            timer = new Timer(service.GetPayTime()*1000);
            timer.Enabled = true;
            timer.AutoReset = true;
            timer.Elapsed += Paid;
            Paid(null, null);
        }
      
        protected virtual void Paid(object sender, ElapsedEventArgs e)
        {
            float fee = GetFee();
            if (Balance < 0)
                fee = fee * service.GetFine();
            Balance -= fee;
            Services.ParkingService.GetInstance().Parking.Transactions.Add(CreateTransaction(Id, fee));
            PaidToParking(fee);
        }

        public virtual void AddMoney(float balance)
        {
            Balance += balance;
        }

        protected virtual TransactionModel CreateTransaction(int Id, float paid)
        {
            return transactionService.CreateTransaction(Id, paid);
        }

        public override string ToString()
        {
            return $"Тип: {GetTransportType()}, Id: {Id}, Баланс: {Balance}";
        }

        public virtual void RefillBalance(float balance)
        {
            Balance += balance;
        }

        public virtual void PaidToParking(float fee)
        {
            Services.ParkingService.GetInstance().Paid(fee);
        }

        public void RemoveTimer()
        {
            timer.Enabled = false;
        }

        public abstract string GetTransportType();
        public abstract float GetFee();
    }
}
