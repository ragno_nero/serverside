﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ServerSide.Transport
{
    class Truck : Transport
    {
        private static float Fee { get; set; }
        private static string Type { get; set; }
        static Truck()
        {
            Fee = service.GetTariff().Truck;
            Type = "Вантажний автомобіль";
        }

        public Truck(float balance) : base(balance)
        { }

        public override float GetFee()
        {
            return Fee;
        }

        public override string GetTransportType()
        {
            return Type;
        }
    }
}
