﻿namespace ServerSide.Transport
{
    class Bus : Transport
    {
        private static float Fee { get; set; }
        private static string Type { get; set; }
        static Bus()
        {
            Fee = service.GetTariff().Bus;
            Type = "Автобус";
        }

        public Bus(float balance) : base(balance)
        { }

        public override float GetFee()
        {
            return Fee;
        }

        public override string GetTransportType()
        {
            return Type ;
        }
    }
}