﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ServerSide.Transport
{
    class Motocycle: Transport
    {
        private static float Fee { get; set; }
        private static string Type { get; set; }
        static Motocycle()
        {
            Fee = service.GetTariff().Motocycle;
            Type = "Мотоцикл";
        }

        public Motocycle(float balance) : base(balance)
        { }

        public override float GetFee()
        {
            return Fee;
        }

        public override string GetTransportType()
        {
            return Type;
        }
    }
}
