﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerSide.Models
{
    public class ParkingModel
    {
        public double Balance { get; set; }
        public List<TransactionModel> Transactions { get; set; }
        public List<Transport.Transport> Transports { get; set; }
        public double Profit { get; set; }
        public int Capacity { get; set; }

    }
}
