﻿using System;
using System.Timers;

namespace ServerSide.Models
{
    public class TransportModel
    {
        public int Id { get; set; }
        public float Balance { get; set; }
        public string Type { get; set; }
    }
}
