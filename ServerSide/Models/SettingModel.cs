﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerSide.Models
{
    public class SettingModel
    {
        public double Balance { get; set; }
        public int Capacity { get; set; }
        public int PayTime { get; set; }
        public float Fine { get; set; }
        public Rate Tariff { get; set; }
    }
}
