﻿using Microsoft.AspNetCore.Mvc;
using ServerSide.Models;
using ServerSide.Services;
using System.Collections.Generic;

namespace ServerSide.Controllers
{

    [ApiController]
    public class ParkingController : ControllerBase
    {
        private ParkingService parkingService;

        public ParkingController()
        {
            parkingService = ParkingService.GetInstance();
        }

        [Route("api/[controller]/balance")]
        public ActionResult<double> GetBalance()
        {
            return Ok(parkingService.GetBalance());
        }

        [Route("api/[controller]/profit")]
        public ActionResult<double> GetProfit()
        {
            return Ok(parkingService.GetProfit());
        }

        [Route("api/[controller]/count")]
        public ActionResult<double> GetCount()
        {
            return Ok(parkingService.GetCount());
        }

        [Route("api/[controller]/transaction/{flag}")]
        public ActionResult<string> GetTransactions(int flag)
        {
            bool f = flag == 1;
            var transactions = parkingService.GetTransactions(f);
            if (transactions != null)
                return Ok(parkingService.GetTransactions(f));
            else return Forbid();
        }

        [Route("api/[controller]/transport")]
        public ActionResult<IEnumerable<TransactionModel>> GetTransport()
        {
            return Ok(parkingService.GetTransports());
        }

        [HttpPost]
        [Route("api/[controller]/transport")]
        public ActionResult AddTransport([FromBody]TransportModel values)
        {
            var flag = parkingService.AddTransport(values);
            if (flag)
                return Ok(225);
            else return Forbid();
        }

        // PUT api/values
        [HttpPut("api/[controller]/transport/{id}")]
        public ActionResult Put(int id, [FromBody] float balance)
        {
            var flag = parkingService.RefillBalance(id, balance);
            if (flag)
                return Ok();
            else return Forbid();
        }

        [HttpDelete("api/[controller]/transport/{id}")]
        public ActionResult Remove(int id)
        {
            var flag = parkingService.DeleteTransport(id);
            if (flag)
                return Ok();
            else return Forbid();
        }
    }
}