﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace ServerSide.Controllers
{
    [Route("api/")]
    [ApiController]
    public class CheckController : ControllerBase
    {
        [HttpGet]
        public ActionResult<string> Get()
        {
            return DateTime.Now.ToString();
        }
    }
}
