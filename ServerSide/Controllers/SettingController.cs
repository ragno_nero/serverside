﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ServerSide.Models;
using ServerSide.Services;

namespace ServerSide.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SettingController : ControllerBase
    {
        private SettingService settingService;

        public SettingController()
        {
            settingService = SettingService.GetInstance();
        }
        // GET api/values
        [HttpGet]
        public ActionResult<SettingModel> Get()
        {
            return Ok(settingService.GetSettings());
        }

        // PUT api/values
        [HttpPut]
        public ActionResult<SettingModel> Put([FromBody] SettingModel values)
        {
            settingService.SetSettings(values);
             return Ok(settingService.GetSettings());
        }
    }
}