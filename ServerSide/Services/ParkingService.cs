﻿using ServerSide.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Timers;

namespace ServerSide.Services
{
    public class ParkingService
    {
        private static ParkingService service;
        public ParkingModel Parking { get; private set; }

        private SettingService setting;
        private TransactionService transactionService;
        private TransportService transportService;
        private Timer timer;

        private ParkingService()
        {
            setting = SettingService.GetInstance();
            transportService = TransportService.GetInstance();
            transactionService = TransactionService.GetInstance();
            Parking = new ParkingModel
            {
                Profit = 0,
                Transactions = new List<TransactionModel>(),
                Transports = new List<Transport.Transport>(),
                Balance = setting.GetBalance(),
                Capacity = setting.GetCapacity()
            };
            timer = new Timer(60000);
            timer.Elapsed += ClearData;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        private void ClearData(object sender, ElapsedEventArgs e)
        {
            transactionService.WriteTransactions(Parking.Transactions);
            Parking.Transactions.Clear();
            Parking.Profit = 0;
        }

        public static ParkingService GetInstance()
        {
            if (service == null)
                service = new ParkingService();
            return service;
        }

        public double GetBalance()
        {
            return Parking.Balance;
        }

        public double GetProfit()
        {
            return Parking.Profit;
        }

        public int GetCount()
        {
            return Parking.Transports.Count;
        }

        public List<TransportModel> GetTransports()
        {
            List<TransportModel> trModel = new List<TransportModel>();
            foreach (var t in Parking.Transports)
            {
                trModel.Add(new TransportModel
                {
                    Id = t.Id,
                    Balance = t.Balance,
                    Type = t.GetTransportType()
                });
            }
            return trModel;
        }

        public string GetTransactions(bool flag)
        {
            if (flag)
                return ArrayToStringFromFile();
            return ListToString();
        }

        public string GetUser(int id)
        {
            return $"user{id}";
        }

        public bool AddTransport(TransportModel model)
        {
            if (Parking.Capacity != Parking.Transports.Count)
            {
                var t = transportService.CreateTransport(model);
                Parking.Transports.Add(t);
                return true;
            }
            return false;
        }

        public bool RefillBalance(int id, float balance)
        {
            return transportService.RefillBalance(id, balance, Parking.Transports);
        }

        public bool DeleteTransport(int id)
        {
            return transportService.RemoveTransport(id, Parking.Transports);
        }

        public void Paid(float fee)
        {
            Parking.Profit += fee;
            Parking.Balance += fee;
        }

        private string ListToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var t in Parking.Transactions)
                stringBuilder.AppendLine("\t" + t.ToString());
            return stringBuilder.ToString();
        }

        private string ArrayToStringFromFile()
        {
            StringBuilder stringBuilder = new StringBuilder();
            var lines = transactionService.ReadTransactions();
            if (lines == null) return null;
                foreach (var t in lines)
                    stringBuilder.AppendLine("\t" + t.ToString());
            return stringBuilder.ToString();
        }
    }
}
