﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using ServerSide.Models;
using Newtonsoft.Json;

namespace ServerSide.Services
{
    public class SettingService
    {
        private static SettingService service;
        public static SettingModel SettingModel { get; private set; }
        private string FileName { get; set; }
        private SettingService()
        {
            FileName = "settings.config";
            if (File.Exists(FileName))
            {
                SettingModel = JsonConvert.DeserializeObject<SettingModel>(File.ReadAllText(FileName));
            }
            else
            {
                SettingModel = new SettingModel
                {
                    Balance = 0,
                    Capacity = 10,
                    Fine = 2.5f,
                    PayTime = 5,
                    Tariff = new Rate
                    {
                        Car = 2f,
                        Truck = 5f,
                        Bus = 3.5f,
                        Motocycle = 1f
                    }
                };
                File.WriteAllText(FileName, JsonConvert.SerializeObject(SettingModel));
            }
        }

        public static SettingService GetInstance()
        {
            if (service == null)
                service = new SettingService();
            return service;
        }

        public SettingModel GetSettings()
        {
            return SettingModel;
        }

        public double GetBalance()
        {
            return SettingModel.Balance;
        }

        public int GetCapacity()
        {
            return SettingModel.Capacity;
        }

        public void SetSettings(SettingModel model)
        {
            SettingModel = model;
        }

        public float GetFine()
        {
            return SettingModel.Fine;
        }

        public int GetPayTime()
        {
            return SettingModel.PayTime;
        }

        public Rate GetTariff()
        {
            return SettingModel.Tariff;
        }
    }
}
