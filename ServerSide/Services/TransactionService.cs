﻿using ServerSide.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace ServerSide.Services
{
    public class TransactionService
    {
        private static TransactionService transactionService;
        private static int Counter = 0;
        private string FileName;

        private TransactionService()
        {
            FileName = "transaction.log";
        }

        public static TransactionService GetInstance()
        {
            if (transactionService == null)
                transactionService = new TransactionService();
            return transactionService;
        }

        public TransactionModel CreateTransaction(int id, float fee)
        {
            ++Counter;
            return new TransactionModel
            {
                CarId = id,
                MoneyAmount = fee,
                Time = DateTime.Now,
                Id = Counter
            };
        } 

        public void WriteTransactions(IEnumerable<TransactionModel> models)
        {
            foreach(var m in models)
                File.AppendAllText(FileName, m + Environment.NewLine);
        }

        public IEnumerable<string> ReadTransactions()
        {
            if (File.Exists(FileName))
            {
                var lines = File.ReadAllLines(FileName);
                return lines;
            }
            return null;
        }
    }
}
