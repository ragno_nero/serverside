﻿using ServerSide.Models;
using System.Collections.Generic;
using System.Linq;

namespace ServerSide.Services
{
    public class TransportService
    {
        private static TransportService transportService;
        private SettingService SettingService { get; set; }

        private TransportService()
        {
            SettingService = SettingService.GetInstance();
        }

        public static TransportService GetInstance()
        {
            if (transportService == null)
                transportService = new TransportService();
            return transportService;
        }

        public Transport.Transport CreateTransport(TransportModel model)
        {
            Transport.Transport transport;
            switch (model.Type)
            {
                case "Car":
                    transport = new Transport.Car(model.Balance);
                    break;
                case "Bus":
                    transport = new Transport.Bus(model.Balance);
                    break;
                case "Truck":
                    transport = new Transport.Truck(model.Balance);
                    break;
                case "Motorcycle":
                    transport = new Transport.Motocycle(model.Balance);
                    break;
                default:
                    transport = null;
                    break;
            }
            return transport;
        }

        public bool RefillBalance(int id,  float balance, List<Transport.Transport> list)
        {
            Transport.Transport transport = list.Where(t => t.Id == id).FirstOrDefault();
            if (transport == null) return false;
            transport.RefillBalance(balance);
            return true;
        }

        public bool RemoveTransport(int id, List<Transport.Transport> list)
        {
            Transport.Transport transport = list.Where(t => t.Id == id).FirstOrDefault();
            if (transport == null) return false;
            if (transport.Balance < 0) return false;
            transport.RemoveTimer();
            list.Remove(transport);
            return true;
        }

    }
}
